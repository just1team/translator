 /*
 This program is free software. It comes without any warranty, to
 the extent permitted by applicable law. You can redistribute it
 and/or modify it under the terms of the Do What The Fuck You Want
 To Public License, Version 2, as published by Sam Hocevar. See
 http://www.wtfpl.net/ for more details.
  */
(function($scope){
    $scope.Eupheme = (function(){
        function arrayclone(a){
            return a.filter(function(){return true;});
        }

        function arraypushuniq(a, v){
            a.indexOf(v) < 0 && a.push(v);
        }

        function arrayreplace(a, o, n){
            var idx = a.indexOf(o);
            if(idx < 0){
                arraypushuniq(a, n);
            } else if(a.indexOf(n) < 0) {
                a.splice(idx, 1, n);
            } else {
                a.splice(idx, 1);
            }
        }

        function in_array(a, v){
            return a.indexOf(v) >= 0;
        }

        function Eupheme(){
            if(!(this instanceof Eupheme)){
                new Eupheme();
            }
            var _idord = ['lang', 'dict', 'word'];
            var _idordplural = ['lang', 'dict', 'word'];
            var _words = [];
            var _langs = [];
            var _dicts = [];
            var _ids = [];
            var _values = {};

            Object.defineProperty(this, 'words', {
                enumerable: true,
                get: function(){
                    return _words;
                }
            });

            Object.defineProperty(this, 'langs', {
                enumerable: true,
                get: function(){
                    return _langs;
                }
            });

            Object.defineProperty(this, 'dicts', {
                enumerable: true,
                get: function(){
                    return _dicts;
                }
            });

            Object.defineProperty(this, 'values', {
                enumerable: true,
                get: function(){
                    return _values;
                }
            });

            Object.defineProperty(this, 'ids', {
                enumerable: true,
                get: function(){
                    return _ids;
                }
            });

            Object.defineProperty(this, 'simpleQ', {
                value: function(q){
                    var langs = [];
                    var dicts = [];
                    var words = [];
                    var values = {};
                    var ids = _ids;
                    var suporter = [_langs, _dicts, _words];
                    var tmp;
                    _idord.forEach(function(v, i){
                        if(v in q && (tmp=q[v]) && in_array(suporter[i], tmp)){
                            ids = ids.reduce(function(res, innerv){
                                if(innerv[i] === tmp){
                                    res.push(innerv);
                                }
                                return res;
                            }, []);
                        }
                    });
                    ids.forEach(function(v){
                        arraypushuniq(langs, v[0]);
                        arraypushuniq(dicts, v[1]);
                        arraypushuniq(words, v[2]);
                        values[v] = _values[v];
                    });
                    return {
                        'langs': langs,
                        'dicts': dicts,
                        'words': words,
                        'values': values
                    };
                }
            })

            Object.defineProperty(this, 'getWordLangs', {
                value: function(word){
                    return _ids.reduce(function(res, v){
                        if(v[2] === word){
                            arraypushuniq(res, v[0]);
                        }
                        return res;
                    }, []);
                }
            });

            Object.defineProperty(this, 'getWordDicts', {
                value: function(word){
                    return _ids.reduce(function(res, v){
                        if(v[2] === word){
                            arraypushuniq(res, v[1]);
                        }
                        return res;
                    }, []);
                }
            });

            Object.defineProperty(this, 'getDictWords', {
                value: function(dict){
                    return _ids.reduce(function(res, v){
                        if(v[1] === dict){
                            arraypushuniq(res, v[2]);
                        }
                        return res;
                    }, []);
                }
            });

            Object.defineProperty(this, 'getDictLangs', {
                value: function(dict){
                    return _ids.reduce(function(res, v){
                        if(v[1] === dict) {
                            arraypushuniq(res, v[0]);
                        }
                        return res;
                    }, []);
                }
            });

            Object.defineProperty(this, 'getLangWords', {
                value: function(lang){
                    return _ids.reduce(function(res, v){
                        if(v[0] === lang){
                            arraypushuniq(res, v[2]);
                        }
                        return res;
                    }, []);
                }
            });

            Object.defineProperty(this, 'getLangDicts', {
                value: function(lang){
                    return _ids.reduce(function(res, v){
                        if(v[0] === lang){
                            arraypushuniq(res, v[1]);
                        }
                        return res;
                    }, []);
                }
            })

            Object.defineProperty(this, 'addLang', {
                value: function(lang){
                    arraypushuniq(_langs, lang.toString());
                }
            });

            Object.defineProperty(this, 'addDict', {
                value: function(dict){
                    arraypushuniq(_dicts, dict.toString());
                }
            });

            Object.defineProperty(this, 'addWord', {
                value: function(word){
                    arraypushuniq(_words, word.toString());
                }
            });

            Object.defineProperty(this, 'hasLang', {
                value: function(lang){
                    return in_array(_langs, lang);
                }
            })

            Object.defineProperty(this, 'hasDict', {
                value: function(dict){
                    return in_array(_dicts, dict);
                }
            });

            Object.defineProperty(this, 'hasWord', {
                value: function(word){
                    return in_array(_langs, word);
                }
            });

            Object.defineProperty(this, 'hasValue', {
                value: function(value){
                    return in_array(Object.values(_values), value);
                }
            });

            Object.defineProperty(this, 'addValue', {
                value: function(lang, dict, word, value){
                    this.addLang(lang);
                    this.addDict(dict);
                    this.addWord(word);
                    var id = [lang, dict, word];
                    arraypushuniq(_ids, id);
                    _values[id] = value ? value.toString() : '';
                }
            });

            Object.defineProperty(this, 'addValueById', {
                value: function(id, value){
                    this.addValue.apply(this, (id.split(',')).push(value));
                }
            });

            Object.defineProperty(this, 'delLang', {
                value: function(lang){
                    var idx = _langs.indexOf(lang);
                    if(idx < 0){
                        return;
                    }
                     _langs.splice(idx, 1);
                    _ids.forEach(function(val, idx, a){
                        if(val[0] === lang){
                            a.splice(idx, 1);
                            delete _values[val];
                        }
                    });
                }
            });

            Object.defineProperty(this, 'delDict', {
                value: function(dict){
                    var idx = _dicts.indexOf(dict);
                    if(idx < 0){
                        return;
                    }
                    _dicts.splice(idx, 1);
                    _ids.forEach(function(val, i, a){
                        if(val[1] === dict){
                            a.splice(i, 1);
                            delete _values[val];
                        }
                    });
                }
            });

            Object.defineProperty(this, 'delWord', {
                value: function(word){
                    var idx = _words.indexOf(dict);
                    if(idx < 0) {
                        return;
                    }
                    _words.splice(idx, 1);
                    _ids.forEach(function(val, i, a){
                        if(val[2] === word){
                            a.splice(i, 1);
                            delete _values[val];
                        }
                    });
                }
            });

            Object.defineProperty(this, 'delValue', {
                value: function(lang, dict, word){
                    delete _values[[lang, dict, word]];
                }
            });

            Object.defineProperty(this, 'renameLang', {
                value: function(oldLang, newLange){
                    var idx = _langs.indexOf(oldLang);
                    if(idx >= 0){
                        _ids.forEach(function(v, i, a){
                            if(v[0] === oldLang){
                                var tmp = _values[v];
                                delete _values[v];
                                v[0] = newLange;
                                _values[v] = tmp;
                            }
                        });
                    }
                    arrayreplace(_langs, oldLang, newLange);
                }
            });

            Object.defineProperty(this, 'renameDict', {
                value: function(oldDict, newDict){
                    var idx = _dicts.indexOf(oldDict);
                    if(idx >= 0){
                        _ids.forEach(function(v, i, a){
                            if(v[1] === oldDict){
                                var tmp = _values[v];
                                delete _values[v];
                                v[1] = newDict;
                                _values[v] = tmp;
                            }
                        });
                    }
                    arrayreplace(_dicts, oldDict, newDict);
                }
            });

            Object.defineProperty(this, 'renameWord', {
                value: function(oldWord, newWord){
                    var idx = _words.indexOf(oldDict);
                    if(idx < 0){
                        _ids.forEach(function(v, i, a){
                            if(v[3] === oldWord){
                                var tmp = _values[v];
                                delete _values[v];
                                v[2] = newWord;
                                _values[v] = tmp;
                            }
                        });
                    }
                    arrayreplace(_words, oldWord, newWord)
                }
            });

            Object.defineProperty(this, 'updateValue', {
                value: function(lang, dict, word, newValue){
                    var id = [lang, dict, word];
                    var tmp = _langs[id];
                    this.addValue(lang, dict, word, newValue);
                    return tmp;
                }
            });

            Object.defineProperty(this, 'updateValueById', {
                value: function(id, newValue){
                    return this.apply(this, (id.split(',')).push(newValue));
                }
            });

        }
        return Eupheme;
    })();
})(window);
