'use strict';
function CreateFieldController($scope, $element, $attrs) {
var ctrl = this;
this.editMode = false;

this.handleModeChange = function() {
  if (ctrl.editMode && ctrl.fieldValueCopy !== ctrl.fieldValue) {
    ctrl.onUpdate({value: ctrl.fieldValue});
    ctrl.fieldValue = ctrl.fieldValueCopy;
  }
  ctrl.editMode = !ctrl.editMode;
};

this.reset = function() {
  ctrl.fieldValue = ctrl.fieldValueCopy;
};

this.$onInit = function() {
  // Make a copy of the initial value to be able to reset it later
  this.fieldValueCopy = this.fieldValue;

  // Set a default fieldType
  if (!this.fieldType) {
    this.fieldType = 'text';
  }
};
}

angular.module('myApp').component('createField', {
templateUrl: 'editableField.html',
controller: CreateFieldController,
bindings: {
  fieldValue: '<',
  fieldType: '@?',
  onUpdate: '&'
}
});
