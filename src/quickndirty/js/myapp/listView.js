'use strict';
function ListViewController($scope, $element, $attrs){
var ctrl = this;

ctrl.updateItem = function(oldValue, newValue) {
  // ctrl.items[index] = value;
  console.log('updating');
  // console.log(index, value);
  ctrl.onUpdate({'oldValue': oldValue, 'newValue': newValue});
};

ctrl.deleteItem = function(value) {
  if (index >= 0) {
    // ctrl.items.splice(index, 1);
    // console.log('eliminazione');
    // console.log(index);
    ctrl.onDelete(value);
  }
};

ctrl.createItem = function(value){
    ctrl.onCreate({'value': value});
}
ctrl.test = function(){
    console.log('testing listView');
    console.log(arguments);
}
}

angular.module('myApp').component('listView', {
templateUrl: 'listView.html',
controller: ListViewController,
bindings: {
    items: '<',
    title: '=',
    onUpdate: '&',
    onDelete: '&',
    onCreate: '&',

}
});
