(function(){
var app = angular.module('myApp', ['ngMdIcons']);
function log(){
    angular.forEach(arguments, function(msg){
        console.log(msg);
    });
}
app.controller('myCtrl', function($scope, $http, $location){
    var eupheme = $scope.eupheme = new Eupheme();
    var ctrlURL = "php/langmanager.php";
    $scope.dicts = eupheme.dicts;
    $scope.langs = eupheme.langs;
    var data = {
        "command": "list"
    };
    $http.post(ctrlURL, data).then(function(response){
        log( response.data);
        var ds = response.data.data.dizionari;
        var ls = response.data.data.lingue;
        // TODO MIGLIORARE IL SISTEMA DI GESTIONE DEL DIZIONARE PER SEMPLIFICARE
        // LA SINCRONIZZAZIONE DEI DATI
        $scope.activeDizionario = ds[0];
        $scope.traduttore = {};
        $scope.words = {};
        $scope.vocaboli = [];
        ls.forEach(function(l){
            eupheme.addLang(l);
            ds.forEach(function(d){
                eupheme.addDict(d);
                $http.get('lang/' + l + '/' + d + '.json').then(function(response){
                    var data = response.data;
                    for(k in data){
                        $scope.eupheme.addValue(l, d, k, data[k]);
                    }
                });
            });
        });
    });
    $scope.editVocabolo = function(key){
        // console.log(key);
        if(!key){
            $scope.activeVocabolo = null;
            $scope.availableLangs = [];
            $scope.availableDicts = [];
            $scope.traduzioni = [];
            return;
        }
        var query = {
            word: key,
            dict: $scope.activeDizionario
        };
        var traduzioni = [];
        var response = eupheme.simpleQ(query);
        $scope.activeVocabolo = key;
        $scope.availableDicts = eupheme.getWordDicts(key);
        $scope.availableLangs = response.langs;
        for(var id in response.values){
            traduzioni.push({
                lingua: id.split(',')[0],
                contenuto: response.values[id]
            });
        }
        $scope.traduzioni = traduzioni;
    };

    $scope.setActiveDizionario = function(dict){
        $scope.activeDizionario = dict;
        $scope.editVocabolo($scope.activeVocabolo || null);
    };

    $scope.addLang = function(lang, dict, key){
        var exists = $scope.traduzioni.reduce(function(res,v){return res || v.lingua === lang}, false);
        if(exists){
            return;
        }
        eupheme.addValue(lang, dict, key, '');
        $scope.editVocabolo(key);
    };

    $scope.salvaVoce = function(lang, dict, key, cont){
        eupheme.updateValue(lang, dict, key, cont);
        var data = {
            "command": "ins",
            "lingua": lang,
            "dizionario": dict,
            "key": key,
            "value": cont
        };
        $http.post(
            ctrlURL,
            data
        ) .then(function(response){
            console.log(response);
        });
    };

    $scope.renameDizionario = function(oldDict, newDict){
        // console.log(arguments);
        eupheme.renameDict(oldDict, newDict);
    }

    $scope.newDict = function(dict){
        dict.length >= 0 && eupheme.addDict(dict);
    }

    $scope.newLang = function(lang){
        lang.length >= 0 && eupheme.addLang(lang);
    }

    $scope.newWord = function(word){
        eupheme.addWord(word);
        $scope.editVocabolo(word);
    }

    $scope.delDict = function(dict){
        eupheme.delDict(dict);
    }

    $scope.delLang = function(lang){
        eupheme.delLang(lang);
    }

    $scope.delWord = function(word){
        eupheme.delWord(word);
    }
    $scope.test = function(){
        console.log(arguments);
    }
});

app.filter('vociFilter', function() {
    return function(input, filter){
        if(!input){
            return null;
        }
        var outkeys = [];
        if(typeof filter === "undefined"){
            outkeys = Object.keys(input);
        } else {
            for(k in input){
                if(k.indexOf(filter) >= 0){
                    outkeys.push(k);
                }
            }
        }
        if(!outkeys){
            return null;
        }
        outkeys.sort();
        var output = outkeys.reduce(function(out, k, idx, self){
            out[k] = input[k];
            return out;
        }, {});
        return output;
    }
});

app.directive('elastic', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
    }
]);

})();
