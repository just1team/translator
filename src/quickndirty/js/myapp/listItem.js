'use strict';
function ListItemController($scope, $element, $attrs) {
    var ctrl = this;

    ctrl.update = function(oldValue, newValue) {
        // console.log('updating');
      ctrl.onUpdate({'oldValue': oldValue, 'newValue': newValue});
    };
    ctrl.delete = function(value){
        ctrl.onDelete({'value': value});
    }
    ctrl.test = function(arg){
        console.log(arg);
    }
}

angular.module('myApp').component('listItem', {
    templateUrl: 'listItem.html',
    controller: ListItemController,
    bindings: {
      value: '<',
      index: '<',
      onUpdate: '&',
      onDelete: '&'
    }
});
