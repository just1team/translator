<?php

/**
 * classe base per la gestione di un dizionario
 */

class LangManager {

    const CMD_CREA_DIZIONARIO = 'crea';
    const CMD_LISTA_DIZIONARI = 'list';
    const CMD_GET_DIZIONARIO = 'get';
    const CMD_INSERISCI = 'ins';
    const CMD_ELIMINA = 'del';

    const NAME_LANGLIST = 'lista_lingue.json';
    const NAME_LISTADIZIONARI = 'lista_dizionari.json';
    const NAME_CONFIG = '.config.json';
    const NAME_KEYFILE = '.lista_chiavi.json';
    const NAME_DEFAULT_DICTIONARY = 'default';

    const DEFAULT_LANG = 'it';
    const LANG_FILE_TYPE = 'json';

    /**
    * directory base dove e' radicato il dizionario
     * @var [type]
     */
    private $_baseDir = null;

    function __construct($base){
        $this->_baseDir = new MyDir($base);
    }

    function createDictionary($lingua, $dizionario=self::NAME_DEFAULT_DICTIONARY){
        if(!is_string($lingua)) {
            throw LangNameException::create("Nome inserito non valido, il nome deve essere una stringa lunga almeno 2 caratteri.", "Param type err");
        } if(strlen($lingua) < 2){
            throw LangNameException::create("Nome inserito non valido, il nome inserito e' troppo corto, la parola deve essere lunga almeno 2 caratteri.", "Param length err");
        }
        $langDir = $this->_baseDir->mkdir($lingua);

        $langlist = $this->_baseDir->openjson(self::NAME_LANGLIST);
        $dictlist = $this->_baseDir->openjson(self::NAME_LISTADIZIONARI);

        $dictlist[] = $dizionario;
        $dictlist->unique();
        $langlist[] = $lingua;
        $langlist->unique();
        // creazione del dizionario su tutte le cartelle
        foreach($langlist->getData() as $ldir){
            $tlangdir = $this->_baseDir->opendir($ldir);
            foreach($dictlist->getData() as $tdizionario){
                $tname = $tdizionario . '.' . self::LANG_FILE_TYPE;
                if(!$tlangdir->hasFile($tname)){
                    $tlangdir->openjson($tname);
                }
            }
        }

    }

    function getAvailableDictionary(){
        $dicts = $this->_baseDir->openjson(self::NAME_LISTADIZIONARI);
        return $dicts->getData();
    }

    function getAvailableLanguagies(){
        $langs = $this->_baseDir->openjson(self::NAME_LANGLIST);
        return $langs->getData();
    }

    function getDictionary($lingua, $dizionario=self::NAME_DEFAULT_DICTIONARY){
        try{
            $langDir = $this->_baseDir->opendir($lingua);
        } catch (Exception $e){
            throw LangNotAvailableException::create("La lingua inserita non e' dispobile. \n" + $e->getMessage());
        }
        $cfg = $langDir->openjson(self::NAME_CONFIG);
        try{
            $dizionario = $langDir->openjson($dizonario);
        } catch (Exception $e) {
            throw DictNotAvailableException::create("Il dizionario scelto non e' disponibile. \n" + $e->getMessage());

        }
        return $d->getData();
    }

    function insert($lingua, $dizionario, $key, $val){
        //TODO creare la funzione per inserire una nuova voce
        $langDir = $this->_baseDir->opendir($lingua);
        $tname = $dizionario . '.' . self::LANG_FILE_TYPE;
        if($langDir->hasFile($tname)){
            $tdizionario = $langDir->openjson($tname);
            $tdizionario[$key] = $val;
            // var_dump($tdizionario);
        } else {
            throw DictNotAvailableException::create("Il dizionario scelto non e' disponibile. \n");
        }

    }

    function delete($lingua, $dizionario, $key){
        //TODO creare la funzione per eliminare una voce
        $langDir = $this->_baseDir->opendir($lingua);
        $tname = $dizionario . '.' . self::LANG_FILE_TYPE;
        if($langDir->hasFile($tname)){
            $tdizionario = $langDir->openjson($tname);
            unset($tdizionario[$key]);
            // var_dump($tdizionario);
        } else {
            throw DictNotAvailableException::create("Il dizionario scelto non e' disponibile. \n");
        }
    }


    function __destruct(){
        if($this->_baseDir){
            $this->_baseDir = null;
        }
    }
}

 ?>
