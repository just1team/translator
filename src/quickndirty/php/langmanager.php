<?php
include_once(".__init__.php");
// var_dump($_SERVER);

if($_SERVER['REQUEST_METHOD'] != 'POST'){
    $res->setError('Metodo della richiesta sbagliata, sono ammesse solo richieste di tipo POST');
    $res->ends();
}
if(empty($_POST)){
    $cmd = json_decode(file_get_contents('php://input'), true);
} else {
    $cmd = $_POST;
}
// 
// $res->addData('requestd', $_REQUEST);
// $res->addData('postd', $cmd);
// $res->addData('errs', error_get_last());
$langmanager = new LangManager(LANG_DIR);

function array_get(array $a, $key, $default=null){
    return isset($a[$key]) ? $a[$key] : $default;
}
try{
    switch ($cmd['command']) {
        case LangManager::CMD_CREA_DIZIONARIO:
            $lingua = array_get($cmd, 'lingua');
            $dizionario = array_get($cmd, 'dizionario');
            if(!$lingua){
                throw Exception("Non e' stato inserito nessuna lingua. Verificare la correttezza della lingua e del dizionario da selezionare.");
            }
            if($dizionario){
                $langmanager->createDictionary($lingua, $dizionario);
            } else {
                $langmanager->createDictionary($lingua);
            }
            $res->addMessage( 'Dizionario creato correttamente');
            break;
        case LangManager::CMD_LISTA_DIZIONARI:
            $dizionari = $langmanager->getAvailableDictionary();
            $lingue = $langmanager->getAvailableLanguagies();
            $res->addData('dizionari', $dizionari);
            $res->addData('lingue', $lingue);
            break;
        case LangManager::CMD_GET_DIZIONARIO:
            $lingua = array_get($cmd, 'lingua');
            $dizionario = array_get($cmd, 'dizionario');
            if(!$lingua){
                throw Exception("Non e' stato inserito nessuna lingua. Verificare la correttezza della lingua e del dizionario da selezionare.");
            } elseif(!$dizionario){
                throw Exception("Non e' stato inserito nessun dizionario. Verificare la correttezza della lingua e del dizionario da selezionare.");
            }
            $d = $langmanager->getDictionary($lingua, $dizionario);
            $res->addData('lingua', $lingua);
            $res->addData('dizionario', $d);
            break;
        case LangManager::CMD_INSERISCI:
            $lingua = array_get($cmd, 'lingua');
            $dizionario = array_get($cmd, 'dizionario');
            $langmanager->createDictionary($lingua, $dizionario);
            $key = array_get($cmd, 'key');
            $cont = array_get($cmd, 'value');
            if(!$lingua){
                throw new Exception("Non e' stato inserito nessuna lingua. Verificare la correttezza dei parametri.");
            } elseif(!$dizionario){
                throw new Exception("Non e' stato inserito nessun dizionario. Verificare la correttezza dei parametri.");
            } elseif(!$key) {
                throw new Exception("Non e' stato inserito nessuna chiave. Verificare la correttezza dei parametri.");
            }
            $langmanager->insert($lingua, $dizionario, $key, $cont);
            break;
        case LangManager::CMD_ELIMINA:
            $lingua = array_get($cmd, 'lingua');
            $dizionario = array_get($cmd, 'dizionario');
            $key = array_get($cmd, 'key');
            if(!$lingua){
                throw new Exception("Non e' stato inserito nessuna lingua. Verificare la correttezza dei parametri.");
            } elseif(!$dizionario){
                throw new Exception("Non e' stato inserito nessun dizionario. Verificare la correttezza dei parametri.");
            } elseif(!$key) {
                throw new Exception("Non e' stato inserito nessuna chiave. Verificare la correttezza dei parametri.");
            }
            $langmanager->delete($lingua, $dizionario, $key);
            break;
    }
} catch( LangNameException $e ) {
    $msg = LangNameException::ERR_NAME . '::' . $e->getType() . ":" . $e->getMessage();
    $res->setError($msg);
} catch( Exception $e ) {
    $res->setError($e->getMessage());
}

$res->ends();
 ?>
