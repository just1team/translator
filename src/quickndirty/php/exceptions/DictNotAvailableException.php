<?php

class DictNotAvailableException extends Exception {
    const ERR_NAME = "DictNotAvailableException";
    private $_type = "DictNotAvailableException";
    function setType($type){
        $this->_type = $type;
    }
    function getType(){ return $this->_type; }

    static function create($msg, $type){
        $e = new DictNotAvailableException($msg);
        if($type){
            $e->setType($type);
        }
        return $e;
    }
}
?>
