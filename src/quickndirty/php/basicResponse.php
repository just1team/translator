<?php
class BasicResponse {

    function __construct(){
        // header('Content-type: application/json');
    }
    private $response = array(
        'errors' => false,
        'errsmessage' => array(),
        'message' => array(),
        'data' => array()
    );

    function addData($key, $data){
        if(is_string($key) && strlen($key) > 0) {
            $this->response['data'][$key] = $data;
        } else {
            $this->response['data'][] = $data;
        }
    }

    function addMessage($msg){
        $this->response['message'][] = $msg;
    }

    function setError($msg){
        $this->response['errors'] = true;
        $this->response['errsmessage'][] = $msg;
    }

    function __toString(){
        return json_encode($this->response);
    }

    function ends(){
        print($this);
        exit();
    }
}

 ?>
