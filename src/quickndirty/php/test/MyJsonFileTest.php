<?php
class MyJsonFileTest extends PHPUnit_Framework_TestCase {
    private $data = <<<JSON
{
    "hello": "hello world!!!"
}
JSON;
    const DATA_NAME = 'test_data.json';

    private $jfile = null;

    public function __construct(...$args){
        // parent::construct(...$args);
        file_put_contents(MyJsonFileTest::DATA_NAME, $this->data);
        $this->jfile =  new MyJsonFile(MyJsonFileTest::DATA_NAME);
    }

    public function testOpen(){
        // var_dump(strrpos('test_data.json', '.json'));
        $jfile = new MyJsonFile(MyJsonFileTest::DATA_NAME);
        // var_dump($jfile);
        $this->assertTrue(isset($jfile['hello']));

        $this->assertEquals($jfile['hello'], 'hello world!!!');
    }

    public function testDelete(){
        // var_dump($this->jfile);
        unset($this->jfile['hello']);
        // var_dump($this->jfile);
        $this->assertTrue(!isset($this->jfile['hello']));
        $this->assertEmpty($this->jfile->getData());
    }

    public function testInsertData(){
        $this->jfile['ciao'] = "ciao mondo!!!";
        $this->assertTrue(isset($this->jfile['ciao']));

        $this->assertEquals($this->jfile['ciao'], 'ciao mondo!!!');
    }

    public function testCreazioneNuovoFileJson(){
        $nome = "nuovo.json";
        $f = new MyJsonFile($nome);
        $f->write();
        $d = json_encode($f->getData());
        //var_dump($d);
        $this->assertFileExists($nome);
        $this->assertJsonStringEqualsJsonFile($nome, $d);
        unlink($nome);
    }

    public function __destruct(){
        // parent::__destruct();
        unlink(MyJsonFileTest::DATA_NAME);
    }
}
 ?>
