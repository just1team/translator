<?php

class MyJsonFile extends MyFile implements ArrayAccess, JsonSerializable {
    private $_container = array();

    private $_needWrite = false;

    public function __construct($path)
    {
        if(!is_string($path)){
            throw new InvalidArgumentException("Il path inserito non e' una stringa.");
        } elseif (empty($path)) {
            throw new InvalidArgumentException("Il path inserito non puo' essere vuoto.");
        } elseif(strrpos($path, '.json') != strlen($path) - 5) {
            throw new UnexpectedValueException("Il path inserito non appartiene a un file json");
        }
        $alreadyExists = file_exists($path);
        if($alreadyExists) {
            $this->_container = json_decode(file_get_contents($path), true);
        }
        parent::__construct($path, 'w');
        $this->_needWrite = true;
    }

    public function getData(){
        // echo __FUNCTION__;
        return $this->_container;
    }

    public function replaceData(array $data){
        $this->_container = $data;
        $this->needWrite = true;

    }

    public function unique(){
        $this->_container = array_unique($this->_container);
                // var_dump($this->_container);
    }

    /**
     * ovvwrite della funzione del parent,
     * non vengono accettati i parametri e scrive questo oggetto dentro il file
     */
    public function write(){
        if($this->_needWrite) {
            parent::write(json_encode($this, JSON_PRETTY_PRINT));
            $this->_needWrite = false;
        }
    }

    /**
     * per conformita' con il file viene restituito questo oggetto
     * sotto forma di stringa json
     * @return string stringa in formato json di questo oggetto
     */
    public function read(){
        return json_encode($this, JSON_PRETTY_PRINT);
    }

    public function readAll(){
        return $this->read();
    }

    /**
     * restituisce sempre 0
     * @return inte restituisce sempre 0
     */
    public function tell(){
        return 0;
    }

    public function seek(){
        return;
    }

    public function rewind(){
        return;
    }

    public function append(){
        $this->write();
    }

    function size(){
        return strlen($this->read());
    }

    function close(){
        parent::close();
    }

    function __destruct(){
        // echo __FUNCTION__;
        $this->write();
        parent::__destruct();
    }

    /* interfaccia per ArrayAccess */
    public function offsetSet($offset, $value){
        if(is_null($offset)){
            $this->_container[] = $value;
            $this->_needWrite = true;
        } else {
            $this->_container[$offset] = $value;
            $this->_needWrite = true;
        }
    }

    public function offsetExists($offset) {
        return isset($this->_container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->_container[$offset]);
        $this->_needWrite = true;
    }

    public function offsetGet($offset) {
        return isset($this->_container[$offset]) ? $this->_container[$offset] : null;
    }

    /* object style access */
    public function __isset($key) {
        return $this->offsetExists($key);
    }

    public function __get($key){
        return $this->offsetGet($key);
    }

    public function __set($key, $value){
        $this->offsetSet($key, $value);
    }

    public function __unset($key){
        $this->offsetUnset($key);
    }

    /* implementazione dell'interfaccia JsonSerializable */
    public function jsonSerialize(){
        return $this->_container;
    }

    public function __toString(){
        return $this->readAll();
    }
}
 ?>
