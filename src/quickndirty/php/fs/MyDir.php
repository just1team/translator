<?php
include_once 'KnownResource.php';
include_once 'NotDirectoryException.php';

class MyDir implements INode {

    /**
     * path della cartella aperta
     * @var string
     */
    private $_path = '';

    /**
     * risorsa aperta da gestire
     * @var resource
     */
    private $_handle = null;

    /**
     * indica se la cartella corrente e' chiusa
     * @var boolean
     */
    private $_closed = true;

    /**
     * contenuto della cartella ottenuto attraverso un scandir
     * @var string[]
     */
    private $_content = null;

    private $_length = 0;

    /**
     * costruttore, crea la risorsa legata alla cartella da gestire
     * locato in path
     * @param string $path locazione della cartella da aprire
     */
    function __construct($path){
        if(is_null($path) || empty($path)){
            $path = '.';
        }
        if(!is_dir($path)) {
            throw new NotDirectoryException("Attenzione: stai tentando di aprire un file che con e' una cartella!!!");
        }
        $this->_path = $path;
        $this->_handle = opendir($path);
        $this->_closed = false;
        $this->_content = scandir($path);
        $this->_length = count($this->_content);
    }

    /**
     * restituisce la voce dell'elemento corrente e porta avanti il cursore
     * @return string nome della risorse indicato dal cursore
     */
    function read(){
        return readdir($this->_handle);
    }

    /**
     * reset del cursore
     */
    function rewind(){
        rewinddir($this->_handle);
    }

    /**
     * effettua una scansione completa di questa cartella
     * e lo restituisce sotto forma di array
     * @return string[] lista del contenuto di questa cartella
     */
    function scan(){
        return $this->_content;
    }

    /**
     * chiude la cartella corrente
     * @return [type] [description]
     */
    function close(){
        closedir($this->_handle);
        $this->_closed = true;
    }

    /**
     * restitusce il path di questa cartella
     * @return string path di questa cartella
     */
    function getPath(){
        return $this->_path;
    }

    function size(){
        return $this->_length;
    }

    function open($filename){
        // var_dump($this->_content);
        if(!in_array($filename, $this->_content)){
            new FileNotExistsException("Il file cercato non e' presente in questa cartella.");
        }
        $path = $this->_path . DIRECTORY_SEPARATOR . $filename;
        // print($path);
        if(is_dir($path)){
            return new MyDir($path . DIRECTORY_SEPARATOR);
        } elseif(is_file($path)) {
            return new MyFile($path);
        }
        throw new UnknownResourceTypeException("La risorsa in " . $path . " non e' riconoscibile." . json_encode(error_get_last()));
    }

    function opendir($dname){
        $dir = $this->open($dname);
        if(!($dir instanceof MyDir)) {
            new Exception("Il file cercato non e' una cartella.");
        }
        return $dir;
    }
    function openfile($fname){
        // var_dump($fname);
        $file = $this->open($fname);
        if(!($file instanceof MyFile)) {
            new Exception("Il file cercato non e' un file.");
        }
        return $file;
    }

    function openjson($jname){
        return new MyJsonFile($this->_path . $jname);
    }

    function mkdir($name){
        //TODO DA CREARE TUTTE LE VERIFICHE DI ERRORI ETC...
        // var_dump($name);
        if(in_array($name, $this->_content)){
            return;
        }
        $path = $this->_path . $name;
        mkdir($path);
        return $this->open($name . DIRECTORY_SEPARATOR);
    }

    function touch($name){
        $path = $this->_path . DIRECTORY_SEPARATOR . $name;
        if(is_file($path)){
            return $this->open($name);
        }
        return new MyFile($path, 'w');
    }

    function hasFile($file){
        $path = $this->_path . DIRECTORY_SEPARATOR . $file;
        // echo $path;
        return is_file($path);
    }

    function hasDir($dir){
        $path = $this->_path . DIRECTORY_SEPARATOR . $dir;
        return is_dir($path);
    }

    function __destruct(){
        // echo "closing dir " . $this->_path . "<br />";
        $this->close();
    }
}
 ?>
