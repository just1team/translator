<?php

/**
 * sotto ispirazione di python os.path
 */

class MyPath {

    private $_dirname = '.' . DIRECTORY_SEPARATOR;

    private $_basename = '';

    private $_extension = '';

    private $_filename = '';

    function __construct(...$args){
        $path = implode(DIRECTORY_SEPARATOR, $args);
        $info = pathinfo($path);
        $this->_basename = $info['basename'];
        $this->_filename = $info['filename'];
        if(isset($info['extension'])){
            $this->_extension = $info['extension'];
        }
        if(isset($info['dirname'])){
            $tdir = self::normalizePath($info['dirname']);
            if($tdir == DIRECTORY_SEPARATOR) {
                $this->_dirname = $tdir;
            } elseif(!empty($tdir)) {
                $this->_dirname = $tdir . DIRECTORY_SEPARATOR;
            }
        }
    }

    function absolute(){
        return realpath($this->join());
    }

    function abspath(){
        return $this->absolute();
    }

    function basename(){
        return $this->_basename;
    }

    function extension(){
        return $this->extension();
    }

    function commonprefix(...$args){

    }

    function dirname(){
        return $this->dirname();
    }

    function exists(){
        return file_exists($this->join());
    }

    function join(){
        return $this->_dirname . $this->_basename;
    }

    function __toString(){
        return $this->join();
    }

    /**
     * @author runeimp@gmail.com
     * funzione ricopiata dalla documentazione di realpath della guida ufficiale php in inglese
     * Dopo aver analizzatto attentamentei le altre risposte ho ritenuto questa come la funzione che
     * rientra di piu' nelle caratteristiche cercate per la ricostruzione di un path.
     *
     * Will convert /path/to/test/.././..//..///..///../one/two/../three/filename
     * to ../../one/three/filename
     *
     * osservando questo esempio si nota come abbia lasciato un path relativo
     * mentre eseguendo un cammino reale nel file system il path sarebbe partito da root
     * credo che questo comportamento rientri di piu' nelle caratteristiche per
     * un costruttore di path in quanto il path inserito potrebbe essere solamente assoluto
     * rispetto a una directory imposta come accade nei server web,
     * anche se in quei casi sarebbe bene non autorizzare al sistema di poter uscire dalla root virtuale.
     *
     *
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    static function normalizePath($path) {
        $parts = array();// Array to build a new path from the good parts
        $path = str_replace('\\', '/', $path);// Replace backslashes with forwardslashes
        $path = preg_replace('/\/+/', '/', $path);// Combine multiple slashes into a single slash
        $segments = explode('/', $path);// Collect path segments
        $test = '';// Initialize testing variable
        foreach($segments as $segment)
        {
            if($segment != '.')
            {
                $test = array_pop($parts);
                if(is_null($test))
                    $parts[] = $segment;
                else if($segment == '..')
                {
                    if($test == '..')
                        $parts[] = $test;

                    if($test == '..' || $test == '')
                        $parts[] = $segment;
                }
                else
                {
                    $parts[] = $test;
                    $parts[] = $segment;
                }
            }
        }
        return implode(DIRECTORY_SEPARATOR, $parts);
    }
}
 ?>
