<?php

interface INode {
    function read();
    function rewind();
    function close();
    function getPath();
    function size();
}
 ?>
