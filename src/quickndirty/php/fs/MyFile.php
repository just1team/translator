<?php

class MyFile implements INode {
    private $_path = '';

    private $_handle = null;

    private $_closed = true;

    private $_length = 0;

    function __construct($path, $mode="r+"){
        //TODO da gestire eventuali error
        $this->_path = $path;
        $this->_handle = fopen($path, $mode);
        $this->_closed = false;
        $this->_length = filesize($this->_path);
    }

    function read(){
        return fgets($this->_handle);
    }

    function readAll(){
        if($this->_length == 0){
            return '';
        }
        return fread($this->_handle, $this->_length);
    }

    function tell(){
        ftell($this->_handle);
    }

    function seek($offset, $whence = SEEK_SET) {
        fseek($this->_handle, $offset, $whence);
    }

    function rewind(){
        rewind($this->_handle);
    }

    function write($data){
        // var_dump($data);
        rewind($this->_handle);
        fwrite($this->_handle, $data);
    }

    function append($data){
        fseek($this->_handle, 0, SEEK_END);
        $this->write($data);
    }

    function getPath(){
        return $this->_path();
    }

    function size(){
        return $this->_length;
    }

    function close(){
        fclose($this->_handle);
        $this->_closed = true;
    }

    function __destruct(){
        // echo "destructin " . $this->_path . "<br />";
        $this->close();
    }
}

 ?>
